const express = require('express');
const bodyParser = require('body-parser');
const ghigh = require('./controller');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/client/getquote', ghigh.getQuote);

app.post('/client/creatshipment', ghigh.createShipment);

app.get('/client/getshipment', ghigh.getShipment);

app.post('/client/deleteshipment', ghigh.deleteShipment);

const server = app.listen(8081, () => {
  const { address, port } = server.address();
  console.log('Example app listening at http://%s:%s', address, port);
});
