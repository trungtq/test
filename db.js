const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('db.db', (err) => {
  if (err) {
    console.error(err);
  }
  console.log('connected to the db database');
});
exports.getPrice = amount => new Promise((resolve, reject) => {
  const sql = 'SELECT Price FROM Rate WHERE weight >=? ORDER BY Price ASC LIMIT 1';
  if (isNaN(amount) || typeof (amount) === 'boolean') reject(amount);
  else {
    db.get(sql, [amount], (err, row) => {
      if (typeof (row) === 'undefined') {
        db.get('select max(price) as cost from Rate', (err1, row1) => {
          if (err1) reject(err1);
          resolve(row1.cost);
        });
      } else {
        resolve(row.price);
      }
    });
  }
});
exports.insertID = ({ data: [{ id, amount }] }) => {
  const sql = 'INSERT INTO Quote(ID,price) VALUES (?,?)';
  db.run(sql, [id, amount], (err, row) => {
    if (err) return err;
    return row;
  });
};
exports.createShipment = ({
  data: {
    quote, origin: { contact: ct1, address: ad1 },
    destination: { contact: ct2, address: ad2 },
  },
}) => new Promise((resolve, reject) => {
  const date = new Date();
  const sql = `insert into shipment(ref,name,email,phone,origin_country_code,
    origin_locality,origin_postal_code,origin_address_line1,origin_organisation,desination_name, 
    desination_email,desination_phone,desination_country_code,desination_locality, 
    desination_postal_code,desination_address_line1,desination_organisation,quoteid,created_at
    ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
  db.run(sql, [Math.floor(Math.random() * 1000000000),
    ct1.name, ct1.email, ct1.phone, ad1.country_code,
    ad1.locality, ad1.postal_code, ad1.address_line1,
    ad1.organisation, ct2.name, ct2.email, ct2.phone,
    ad2.country_code, ad2.locality, ad2.postal_code, ad2.address_line1,
    ad2.organisation, quote.id, date.toString()], (err, row) => {
    if (err) reject(err);
    return row;
  });
  const sql2 = 'select quote.price,shipment.created_at,shipment.ref from quote inner join shipment on quote.id =shipment.quoteid where id =?';
  db.get(sql2, quote.id, (err, row) => {
    if (err) reject(err);
    resolve(row);
    return row;
  });
});
exports.getShipment = data => new Promise((resolve, reject) => {
  const sql = 'SELECT * FROM shipment WHERE ref =?';
  const obj = {
    data: {
      ref: '',
    },
  };
  db.get(sql, [data], (err, row) => {
    if (err) reject(err);
    return row ? resolve(row) : resolve(obj);
  });
});
exports.deleteShipment = ({ data: { ref: data } }) => new Promise((resolve, reject) => {
  const sql = 'DELETE FROM shipment WHERE ref =?';
  const OK = {
    data: [
      {
        status: 'OK',
        message: 'shipment has been deleted',
      }],
  };
  const sql1 = 'SELECT ref FROM shipment WHERE ref =?';
  db.get(sql1, [data], (err, row) => {
    if (row) {
      db.run(sql, [data], () => {
        resolve(OK);
        return true;
      });
    } else {
      db.run(sql, [data], () => {
        OK.data[0].status = 'NOK';
        OK.data[0].message = 'Shipment not found';
        reject(OK);
        return true;
      });
    }
  });
});

