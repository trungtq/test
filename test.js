// const chai = require('chai');

// const expect = chai.expect;

// chai.should();

// const gp = require('../db');

// describe('1st unit test', () => {
//   it('it should fulfill', () => {
//     gp.getPrice(17000).then((result) => {
//       result.to.equal(100);
//     }).catch(err => err);
//   });
//   it('it should reject', () => {
//     gp.getPrice('a').catch((result) => {
//       result.to.equal(NaN);
//     });
//   });
//   it('it should reject', () => {
//     gp.getPrice(true).catch((result) => {
//       result.to.equal(true);
//     });
//   });
// });
// blank line
const chai = require('chai');

const expect = chai.expect;
const gp = require('../db');

describe('#Promise', () => {
  it('it should add', (done) => {
    gp.getPrice('a').then((result) => {
      expect(result).to.equal(NaN);
    }).catch(() => done(), done);
  });
  it('it should reject', (done) => {
    gp.getPrice(true).catch((err) => {
      expect(err).to.equal(true);
    }).then(() => done())
      .catch(err => done(err));
  });
});
// const chai = require('chai');
// const chaiHttp = require('chai-http');
// const server = require('../db');

// const should = chai.should();

// chai.use(chaiHttp);
// // Our parent block
// describe('Pets', () => {
//   beforeEach((done) => {
//     done();
//   });
//   describe('/GET pets', () => {
//     it('it should GET all the pets', (done) => {
//       chai.request(server)
//         .get('/client/getshipment')
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.body.should.be.a('array');
//           res.body.length.should.be.eql(9);
//           done();
//         });
//     });
//   });
// });
